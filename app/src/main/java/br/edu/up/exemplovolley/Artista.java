package br.edu.up.exemplovolley;

/**
 * Universidade Positivo
 * Bacharelado em Sistemas de Informação
 * Criado por Geucimar Brilhador
 * 21/10/2015
 */
public class Artista {

    private int id;
    private String idGoogle;
    private String nome;

    Artista(int id, String nome){
        this.id = id;
        this.nome = nome;
    }

    Artista(String idGoogle, String nome){
        this.idGoogle = idGoogle;
        this.nome = nome;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIdGoogle() {
        return idGoogle;
    }

    public void setIdGoogle(String idGoogle) {
        this.idGoogle = idGoogle;
    }
}
