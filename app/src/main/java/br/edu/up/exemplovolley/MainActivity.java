package br.edu.up.exemplovolley;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void carregarJSON(View view) {

        String url = "http://player-mp3.appspot.com/ws/artistas";
        StringRequest request = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String json) {
                TextView txt = (TextView) findViewById(R.id.txtJson);
                txt.setText(json);

                converterJSONParaObjetos(json);


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(MainActivity.this, "Deu erro!", Toast.LENGTH_LONG).show();
                    }
                }
        );

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(request);

    }

    private void converterJSONParaObjetos(String json) {

        try {

            ArrayList<Artista> listaDeArtistas = new ArrayList<>();

            JSONArray jsonArray = new JSONArray(json);


            for(int i = 0; i <= jsonArray.length(); i++){
                JSONObject jsonObject = jsonArray.getJSONObject(i);

                String idGoogle = jsonObject.getString("id");
                String nome = jsonObject.getString("nome");

                Artista artista = new Artista(idGoogle, nome);
                listaDeArtistas.add(artista);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}










